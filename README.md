# Mixer Stats &middot; [![version][version-url]](https://semver.org) [![licence][license-url]]() [![commits][commits-url]]() [![pipeline][pipeline-url]]()

Mixer Stats is a web application to do mathematical operations
## Getting Started / Installing

Command to install all of the dependencies, build the project then start it

```
make install && make build && make start
```

### Prerequisites

Make sure you have installed all of the following prerequisites on your development machine : 

* Git - [Download & Install Git](https://git-scm.com/downloads). OSX and Linux machines typically have this already installed.

* Docker - [Download & Install Docker](https://docs.docker.com/v17.09/engine/installation/#supported-platforms). OSX and Linux machines typically have this already installed.

## Built With

* [Symfony 4](https://symfony.com/doc/current/index.html) - The web framework used
* [PHP 7.3](https://symfony.com/doc/current/index.html) - The web language used
* [MariaDB](https://mariadb.com/kb/en/library/documentation/) - Database management system
* [Nginx](https://nginx.org/en/docs/) - Web server used
* [PHP-FPM](https://www.php.net/manual/fr/install.fpm.php) - Used to make the connection between the web server and PHP

## Running the tests

Command to run unit tests :
```
make test_unit
```

Command to run end to end tests :
```
make test_e2e
```

Command to run both tests : 
```
make tests
```

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

## Authors

| <a href="https://gitlab.com/christophele" target="_blank">**Christophe LE**</a> | <a href="https://gitlab.com/bvasseur77" target="_blank">**Baptiste VASSEUR**</a> | <a href="https://gitlab.com/alexiskn" target="_blank">**Alexis FAIZEAU**</a> | <a href="https://gitlab.com/kamISKRANE" target="_blank">**Kamel ISKRANE**</a> | <a href="https://gitlab.com/kiliandiogo" target="_blank">**Killian DIOGO**</a>
| :---: |:---:|:---:|:---:| :---:|
| [![Mixer Stats](https://gitlab.com/uploads/-/system/user/avatar/1408322/avatar.png?width=200)](https://gitlab.com/christophele)    | [![Mixer Stats](https://gitlab.com/uploads/-/system/user/avatar/3786758/avatar.png?width=400)](https://gitlab.com/bvasseur77) | [![Mixer Stats](https://secure.gravatar.com/avatar/55448a255c889a083abd8ee8a9b4083a?s=80&d=identicon)](https://gitlab.com/alexiskn)  | [![Mixer Stats](https://gitlab.com/uploads/-/system/user/avatar/3603263/avatar.png?width=400)](https://gitlab.com/kamISKRANE) | [![Mixer Stats](https://secure.gravatar.com/avatar/6e610014305ee861da9cb4bd84a6dbba?s=180&d=identicon)](https://gitlab.com/kiliandiogo) |
| <a href="https://gitlab.com/christophele" target="_blank">`https://gitlab.com/christophele`</a> | <a href="https://gitlab.com/bvasseur77" target="_blank">`https://gitlab.com/bvasseur77`</a> | <a href="https://gitlab.com/alexiskn" target="_blank">`https://gitlab.com/alexiskn`</a> | <a href="https://gitlab.com/kamISKRANE" target="_blank">`https://gitlab.com/kamISKRANE`</a> | <a href="https://gitlab.com/kiliandiogo" target="_blank">`https://gitlab.com/kiliandiogo`</a> |

## License

This project is licensed under the GNU General Public License v3.0 License - see the [LICENSE.md](LICENSE.md) file for details

<!-- MARKDOWN LINKS & IMAGES -->
[license-url]: https://img.shields.io/badge/dynamic/json?color=brightgreen&label=license&query=license&url=https%3A%2F%2Fgitlab.com%2Fnolway%2Fmixer-stats%2Fraw%2Fdevelop%2Fcomposer.json
[version-url]: https://img.shields.io/badge/dynamic/json?color=blue&label=version&query=release_tag&url=https%3A%2F%2Fgitlab.com%2Fnolway%2Fmixer-stats%2Fraw%2Fdevelop%2Fbadges.json
[commits-url]: https://img.shields.io/badge/dynamic/json?color=brightgreen&label=commits&query=commits&url=https%3A%2F%2Fgitlab.com%2Fnolway%2Fmixer-stats%2Fraw%2Fdevelop%2Fbadges.json
[pipeline-url]: https://gitlab.com/nolway/mixer-stats/badges/develop/pipeline.svg
