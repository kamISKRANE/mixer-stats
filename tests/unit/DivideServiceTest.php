<?php

namespace App\Tests\Unit;

use App\Service\Math\DivideService;
use PHPUnit\Framework\TestCase;

class DivideServiceTest extends TestCase
{
    public function testCalc()
    {
        $firstNum = 5;
        $secondNum = 2;
        $service = new DivideService();
        $result = $service->calc($firstNum, $secondNum);

        $this->assertSame(2.5, $result);
    }
}
