<?php

namespace App\Tests\Unit;

use App\Service\Math\DiffService;
use PHPUnit\Framework\TestCase;

class DiffServiceTest extends TestCase
{
    public function testCalc()
    {
        $firstNum = 7;
        $secondNum = 2;
        $service = new DiffService();
        $result = $service->calc($firstNum, $secondNum);

        $this->assertSame(5.0, $result);
    }
}
